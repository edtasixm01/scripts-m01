#!/bin/bash
#Nom: Raul Baena Nocea
#Data: 21-1-2018
#Curs:ASIX
#Validar nota for
#Synopsis: prog.sh arg1 ...
#--------------------------------------------------------------------------------------------------------------------------------------------------------------

#CONSTANTS
ERR_NARGS=1
ERR_NUMS=2
OK=0
#Validar num args
if [ $# -lt 1 ]
then
  echo "Error: numero d'arguments invalid"
  echo "usage: prog.sh arg1"
  exit $ERR_NARGS
fi

llistanotes=$*
#Validar nota
for nota in $llistanotes
do
  if [ $nota -lt 0 -o $nota -gt 10 ]
  then
    echo "Error: La nota $nota no es valida" >> /dev/stderr
    echo "Usage: Posar un numero entre 1 y el 10" >> /dev/stderr
    echo "Usage:  $ prog nota..." >> /dev/stderr
  else
    if [ $nota -lt 5 ]
    then
      msg="$var: Suspes"
    elif [ $nota -lt 7 ]
    then
      msg="$var Suficient"
    elif [ $nota -le 9  ]
    then
      msg="$var Notable"
    else
      msg="$var Excelent"
    fi
    echo "La nota $nota és: $msg"
 fi
done
exit $OK
