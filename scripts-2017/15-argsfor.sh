#! /bin/bash
# @edt ASIX-M01 Curs 2017-2018
# gener 2018
# Descripcio: separar opcions i args
# synopsis: $ prog [ -a -b -c -d -e ] args...
# -----------------------------------------
OK=0
llistaOpc=""
llistaArg=""
for arg in $*
do
  case $arg in 
    -[abcde])
      llistaOpc="$llistaOpc $arg";;
    *)
      llistaArg="$llistaArg $arg";;
  esac
done
echo "opcions: $llistaOpc"
echo "arguments: $llistaArg"
exit $Ok
