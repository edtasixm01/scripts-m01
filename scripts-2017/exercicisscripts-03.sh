#!/bin/bash
#@edt ASIX-M01 2017-2018
#02/02/2018
#Descripcio: mostrar matricules bones
#Synopsis: prog matricula...
#####################################################
ERR_NARG=1
status=0
#descartem nombre d'arguments incorrectes
if [ $# -eq 0 ]
then
  echo "err: nombre de d'arguments incorrectes"
  echo "usage: prog matricula..."
  exit $ERR_NARG
fi
#iterem per cada matricula
for matricula in "$@"
do  
  echo $matricula | egrep "^[0-9]{4} [A-Z]{3}$" 2> /dev/null
  if [ $? -ne 0 ] 
  then
    status=$((status+1))
    echo $matricula >> /dev/stderr
  fi
done
exit $status
