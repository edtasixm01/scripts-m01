#! /bin/bash
# @edt ASIX-M01 Curs 2017-2018
# gener 2018
# Descripcio: exemples case
# ---------------------------------

lletra=$1
case $lletra in
  'a'|'e'|'i'|'o'|'u')
    echo "$lletra es una vocal";;
  *)
    echo "$lletra es una consonant";;
esac
exit 0

nom=$1
case $nom in 
  "anna"|"marta")
    echo "es una nena";;
  "pere"|"pau")
    echo "es un nen";;
  *)
    echo "es una altra cosa";;
esac



