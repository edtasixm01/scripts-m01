#!/bin/bash
# @edt ASIX-M01 Curs 2017-2018
# gener 2018
# Descripcio: comptar quants arguments tenen 3 o més caracters
# synopsis: $ prog arg...
#---------------------------------------------------
#1)
ERR_NARGS=1
OK=0
if [ $# -lt 1 ]
then
  echo "usage: numero d'arguments no vàlid"
  echo "usage: introdueix un o més arguments"
  exit $ERR_NARGS
fi
#2)
llistaarg=$*
contador=0
for arg in $llistaarg
do
  echo "$arg" | egrep "^.{3,}$" &> /dev/null
  if [ $? -eq 0 ]
  then 
    contador=$(($contador+1))
  fi
done
echo "$contador"
exit $OK

