#! /bin/bash
# @edt ASIX-M01 Curs 2017-2018
# gener 2018
# Descripcio: exemples while
# ---------------------------------
num=1
while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
  num=$((num+1))
done
exit 0

read -r line
while [ $line != "FI" ]
do
  echo $line
  read -r line
done
exit 0

while read -r line
do 
  echo $line
done
exit 0

while [ $# -gt 0 ]
do
  echo "$1"
  shift
done
exit 0

num=1
while [ -n "$1" ]
do
  echo "$num: $1"
  num=$((num+1))
  shift
done
exit 0

while [ -n "$1" ]
do
  echo "$1"
  shift
done
exit 0

MIN=0
num=$1
while [ $num -ge $MIN ]
do
  echo -n "$num, "
  num=$((num-1))
done
echo ""
exit 0

MAX=10
num=0
while [ $num -le $MAX ]
do
  echo "$num"
  num=$((num+1))
done

