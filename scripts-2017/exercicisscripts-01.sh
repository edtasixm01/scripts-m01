#! /bin/bash
# @edt ASIX M01 2017-18
# Programa que procesa los argumentos suministrados y muestra los que tengan 4 o mas caracteres 
# synopsis: ejprocesargs1.sh arg1...
#----------------------------
ERR_NARGS=1
if [ $# -lt 1 ]
then
  echo "ERROR, debe suministrar al menos un argumento"
  echo "usage: ejprocesargs1.sh arg1..."
  exit $ERR_NARGS
fi
for arg in $*
do
  echo $arg | egrep "^.{4,}$"
done
