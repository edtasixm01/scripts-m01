#!/bin/bash
#@edt ASIX-M01 2017-2018
#02/02/2018
#Descripcio: mostrar stdin per stdout les linias de menys de 50 char
#Synopsis: prog stdin
#####################################################
OK=0

#llegim stdin
while read -r line
do
  echo $line | egrep "^.{,49}$"
done 
exit $OK
