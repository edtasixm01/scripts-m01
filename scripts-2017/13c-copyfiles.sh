#! /bin/bash
# @edt ASIX-M01 Curs 2017-2018
# gener 2018
# Descripcio: copiar files a dir desti
# synopsis: $ prog file... dirDesti
# ---------------------------------
OK=1
ERR_DIR=2
ERR_NARGS=1
status=$OK
#1) validar si help
if [ $# -eq 1 -a "$1" = "-h" ]
then
  echo "@edt ASIX-M01 Curs 2017-2018"
  echo "usage: $prog file... dirDesti"
  exit $OK
fi
#2) Validar num args
if [ $# -lt 2 ]
then
  echo "Error num args"
  echo "usage: $prog file... dirDesti"
  exit $ERR_NARGS
fi
dirDesti=$(echo $*| sed 's/^.* //')
llistaFiles=$(echo $* | sed 's/ [^ ]*$//')
#3) Validar es un dir (si no err)
if [ ! -d "$dirDesti" ]
then
  echo "Error arg is not dir"
  echo "usage: $ prog file... dirDesti"
  exit $ERR_DIR
fi
#4) Iterar file a file
for file in $llistaFiles
do
  if [ ! -f "$file" ]
  then
    echo "Error: file $file is not a regular file" >> /dev/stderr
    status=$((status+1))
  else
    echo "cp $file $dirDesti"
  fi 
done
exit $status
