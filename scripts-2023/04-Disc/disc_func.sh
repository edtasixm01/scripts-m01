#!/bin/bash
# exemples de funcions de disc
# @edt Abril 2024
# ------------------------------------
function fsize(){
  login=$1
  line=$(grep "^$login:" /etc/passwd)
  if [ -z "$line" ]; then
     echo "error:...."
     return 1
  fi	  
  dirHome=$(echo $line | cut -d: -f6)
  du -sh $dirHome
}

# rep arguments que són logins
# i calcula el fsize del home
function loginargs(){
  if [ $# -eq 0 ]; then
    echo "error...."
    return 1
  fi
  for login in $*
  do
    fsize  $login
  done	  
}

# rep un argument que és un fitxer
# cada línia del fitxer té un login
function loginfile(){
  if [ ! -f $1 ]; then
    echo "error..."
    return 1
  fi
  fileIn=$1
  while read -r login
  do	
    fsize $login
  done < $fileIn  
}

# rep logins per la entrada standard
# per a cada un d'ells calcula fsize
function loginstdin(){
  while read -r login
  do
    fsize $login
  done	  
}

# processa el fitxer rebut o 
# stdin si no en rep cap
function loginboth(){
  fileIn="/dev/stdin"
  if [ $# -eq 1 ]; then
    fileIn=$1
  fi

  while read -r login
  do
    fsize $login
  done < $fileIn	  
}



