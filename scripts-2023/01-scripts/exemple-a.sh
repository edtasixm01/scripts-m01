#!/ bin/bash
# prog.sh file
#  file conté maricules
#  -------------------
#1) validar num args
#
#
status=0
fileIn=$1
while read -r line
do	
  echo $line | grep -E "^[A-Z,a-z]{4}[0-9]{3}$"
  if [ $? -ne 0 ]; then
    echo "$line" >> /dev/stderr
    status=3
  fi	  
done < $fileIn
exit $status

