! /bin/bash
# @ edt ASIX-M01 Curs 2023-2024
# $ prog file
# indicar si dir és: regular, dir, link 
# o altra cosa
# --------------------------------------
# 1) si num args no es correcte plegar
ERR_NARGS=1
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 fit"
  exit $ERR_NARGS
fi
# 2) xixa
fit=$1
if [ ! -e $fit  ]; then
  echo "$fit no existeix"
  exit $ERR_NOEXIST  
elif [ -f $fit ]; then
  echo "$fit és un regular file"
elif [ -h $fit ]; then
  echo "$fit és un link"
elif [ -d $fit ]; then
  echo "$fit és un directori"
else
  echo "$fit és una altra cosa"	
fi
exit 0

