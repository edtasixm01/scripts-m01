#! /bin/bash
# @ edt ASIX-M01 Curs 2023-2024
# Febrer 2024
# Validar nota: suspès, aprovat
#     a) rep un argument
#     b) és del 0 al 10
# -------------------------------
ERR_NARGS=1
ERR_NOTA=2
# 1) si num args no es correcte plegar
if [ $# -ne 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi
#2) Validar nota [0,10]
if ! [  $1 -ge 0 -a $1 -le 10 ] 
then
  echo "Error nota $1 no valida"
  echo "nota pren valors del 0 al 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi
# 3) xixa
nota=$1
if [ $nota -lt 5 ]
then
  echo "Nota $nota: suspès"
else
   echo "Nota $nota: aprovat"
fi
exit 0



