#! /bin/bash
# @edt ASIX-M01 Curs 2023-2024
# Febrer 2024
# Descripcio: dir els dies que té un més
# Synopsis: prog mes
#    a) validar rep un arg
#    b) validar mes [1-12]
#    c) xixa
# ---------------------------------
ERR_NARGS=1
ERR_ARGVL=2
#1) Validar existeix un arg
if [ $# -ne 1 ]
then
  echo "Error, numero d'arguments no valid"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi
#2) Validar el més pren valors [1-12]
mes=$1
if ! [ $mes -ge 1 -a $mes -le 12  ]
then
  echo "Error, mes $mes no vàlid"
  echo "Mes pren valors del [1-12]"
  echo "Usage: $0 mes"
  exit $ERR_ARGVL
fi
#3) xixa
case $mes in
  "2")
     dies=28;;	  
  "4"|"6"|"9"|"11")
     dies=30;;	  
  *)
     dies=31;;	   
esac	
echo "El més $mes, té $dies dies"
exit 0






