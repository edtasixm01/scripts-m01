#! /bin/bash
# @edt Febrer 2024
# exemples while
# ------------------------
# 8) processar linia a linia un file
fileIn=$1
num=1
while read -r line
do
  chars=$(echo $line | wc -c)
  echo "$num: ($chars)  $line" | tr 'a-z' 'A-Z'
  ((num++))
done < $fileIn
exit 0

#7) numerar i mostrar en majúscule
#   stdin
num=1
while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
  ((num++))
done
exit 0

#6) itera linia linia fins a token
#   (per exemple FI)
TOKEN="FI"
num=1
read -r line
while [ "$line" != $TOKEN ]
do
  echo "$num: $line"
  read -r line
  ((num++))
done
exit 0

#5) numerar les linies rebudes
num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done	
exit 0

# 4) processar stdin linia a linia
while read -r line
do
  echo $line
done	
exit 0

# 3) Iterar la llista d'arguments
#    (oju! usar normalment for)
while [ -n "$1" ]
do
  echo "$1 $# $*"
  shift  
done
exit 0

# 2) comptador decrementa valor rebut
#    narg...0
MIN=0
num=$1
while  [ $num -ge $MIN  ]
do
  echo -n "$num "
  ((num--))
done
exit 0

# 1) mostrar numeros del 1 al 10
MAX=10
num=1
while [  $num -le $MAX ]
do
  echo -n "$num "
  ((num++))
done
exit 0


