#! /bin/bash
# @edt Març 2024
# funcions
# ---------------

function suma(){
  echo $(($1+$2))
  return 0
}

function showUserByLogin(){
  # 1) validar n args
  # 2) validar login  existeix
  login=$1
  line=$(grep "^$login:" /etc/passwd)
  if [ -z  "$line" ];then
    echo "error..."
    return 1
  fi
  uid=$(echo $line | cut -d: -f3)
  gid=$(echo $line | cut -d: -f4)
  shell=$(echo $line | cut -d: -f7)
  echo "login: $login"
  echo "uid: $uid"
  echo "gid: $gid"
  echo "shell: $shell"
  return 0
}

function showUsersInGroupByGid(){
  # 1) validar args
  # 2) validar existeix grup
  #    mostrar gname(gid)
  gid=$1
  gname=$(grep "^[^:]*:[^:]*:$gid:" \
	  /etc/group | cut -d: -f1  ) 
  if [ -z "$gname" ]; then
    echo "error..."
    return 1
  fi
  echo "Grup: $gname($gid)"  
  # 3)login, uid, shell <-- gid
  grep "^[^:]*:[^:]*:[^:]*:$gid:" \
      /etc/passwd | cut -d: -f1,3,7
}

function showAllShells(){
  MIN=$1
  llista_shells=$(cut -d: -f7 /etc/passwd \
      | sort | uniq)
  for shell in $llista_shells
  do
    num_users=$(grep ":$shell$" /etc/passwd \
       | wc -l)
    if [ $num_users -gt $MIN ]; then
      echo "Shell: ($num_users) $shell"
      grep ":$shell$" /etc/passwd \
       | cut -d: -f 1,3,4 \
       | sed -r 's/^(.*)$/\t\1/' 
    fi  
  done
  return 0  
}




