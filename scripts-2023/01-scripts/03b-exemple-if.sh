#! /bin/bash
# @edt ASIX-M01
# Gener 2024
#
# Exemples ordre if
# ---------------------------------------
#
# 1) Validar arguments
if [ $# -ne 1 ]
then
  echo "Error: num args incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi	

# 2) xixa
edat=$1
if [ $edat -lt 18 ]
then
  echo "edat $edat és menor d'edat"
else
  echo "edat $edat és major d'edat"
fi
exit 0





