#! /bin/bash
# @edt ASIX M01-ISO
# prog dir
#  a) rep un arg i és un directori i es llista
#  b) llitar numerant els elements del dir
#  c) per cada element dir si es dir, regular,
#     o altra cosa
# ----------------------------------------------
#1) validar que hi ha un arg
ERR_NARGS=1
ERR_NODIR=2
# 1) validar arguments
if [ $# -ne 1 ]; then
  echo "Error: número args no vàlid"
  echo "usage: $0 dir"
  exit $ERR_NARGS
fi
dir=$1

#2) validar que és un dir
if  [ ! -d $dir  ]; then
  echo "Error: $dir no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi

#3) xixa: llistar
llista_dir=$(ls $dir)
for elem in $llista_dir
do
  if [ -f  $dir/$elem ]; then
    echo "$elem is regular file"
  elif [ -d $dir/$elem ]; then
    echo "$elem is a directory"
  else
    echo "$elem is another thing"
  fi    
done
exit 0


