#! /bin/bash
# @ edt ASIX-M01 Curs 2023-2024
#
# llistar el directori rebut
#    a) verificar rep un arg
#    b) verificar que és un directori
# -------------------------------
ERR_ARGS=1
ERR_NODIR=2
# 1)si num args no es correcte plegar
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir"
  exit $ERR_ARGS
fi
# 2)si no és un directori plegar
if [ ! -d $1  ]; then
  echo "ERROR: $1 no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi	
# 3) xixa
ls $1
exit 0


