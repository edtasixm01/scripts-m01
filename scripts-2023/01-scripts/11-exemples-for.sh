#! /bin/bash
# @edt ASIX-M01 Curs 2023-2024
# Febrer 2024
# Descripcio: exemples bucle for
# ---------------------------------
#8) Llistar els login ordenats i numerats
# cut -d: -f1 /etc/passwd | sort 
llista_logins=$(cut -d: -f1 /etc/passwd | sort)
num=1
for login in $llista_logins
do
  echo "$num: $login"
  ((num++))
done	
exit 0 

# 7) llistar numerant les linies
num=1
llistat=$(ls)
for elem in $llistat
do
  echo "$num: $elem"
  ((num++))
done	
exit 0

# 6) iterar pel resultat d'executar
#    la ordre ls
llistat=$(ls)
for elem in $llistat
do
  echo "$elem"
done	
exit 0

# 5) numerar arguments
num=1
for arg in $*
do
  echo "$num: $arg"
  ((num++))
done
exit 0

# 4) $@ expandeix $* no
for arg in "$@"
do
  echo "$arg"
done
exit 0

#3) iterar per la llista d'arguments
for arg in "$*"
do
  echo "$arg"
done
exit 0

#2) iterar noms
for nom in "pere marta anna pau"
do
  echo "$nom"
done	
exit 0


#1) iterar noms
for nom in "pere" "marta" "anna" "pau"
do
  echo "$nom"
done	
exit 0

