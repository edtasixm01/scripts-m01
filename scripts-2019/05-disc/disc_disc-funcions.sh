#! /bin/bash
# @edt ASIX M01-ISO
# funcions de disc
# ###########################################

#(1) Donat un login calcular amb du l'ocupació del home de lusuari. Cal obtenir el home del /etc/passwd. Validar que es rep un argument i que aquest és un lògin vàlid.
function fsize(){
  login=$1
  home=""
  home=$(grep "^$login:" /etc/passwd | cut -d: -f6)
  if [ -z "$home" ]; then
    echo "ERR: user $login not exists" >> /dev/stderr
    return 1
  fi
  du -sh $home 2> /dev/null 
  return 0
}

#(2) Aquesta funció rep logins i per a cada login es mostra la ocupació de disc del home de l'usuari usant fsize.Verificar que es rep almenys un argument. Per a cada argument verificar si és un login vàlid, si no generra una traça d'error.
function loginargs(){
  for login in $*
  do
    fsize $login 
  done 
}

#(3) Rep com a argument un nom de fitxer que conté un lògin per línia. Mostrar l'ocupació de disc de cada usuari usant fsize. Verificar que es rep un argument i que és un regular file.
function loginfile(){
  fileIn=$1
  while read -r login
  do
    fsize $login
  done < $fileIn
}

#(4) Rep com a argument un file o res (en aquest cas es processa stdin). El fitxer o stdin contenen un lògin per línia. Mostrar l'ocupació de disc del home de l'usuari. Verificar els arguments rebuts. verificar per cada login rebut que és vàlid.
function loginboth(){
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then
    fileIn=$1
  fi
  while read -r login
  do
    fsize $login
  done < $fileIn
}

#(5) Donat un GID com a argument, llistar els logins dels usuaris que petanyen a aquest grup com a grup principal. Verificar que es rep un argument i que és un GID vàlid.
function grepgid(){
  if [ $# -ne 1 ]; then
    return 1
  fi
  gid=$1
  grep "^[^:]*:[^:]*:$gid:" /etc/group &> /dev/null
  if [ $? -ne 0 ]; then 
    return 2
  fi
  grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd \
       | cut -d: -f1
}

#(6) Donat un GID com a argument, mostrar per a cada usuari que pertany a aquest grup l'ocupació de disc del seu home. Verificar que es rep un argument i que és un gID vàlid.
function gidsize(){
  # validar...
  llistaLogins=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" \
       /etc/passwd | cut -d: -f1 | sort 
  for login in $llistaLogins
  do
    fsize $login	
  done
}

#(7) Llistar de tots els GID del sistema (en ordre creixent) l'ocupació del home dels usuaris que hi pertanyen.
function allgidsize(){
  llistaGids=$(cut -d: -f4 /etc/passwd | sort -g | uniq)
  for gid in $llistaGids
  do
    gidsize $gid 
  done  
}

#(8) Llistar totes les línies de /etc/group i per cada llínia llistar l'ocupació del home dels usuaris que hi pertanyen. Ampliar filtrant només els grups del 0 al 100
function allgroupsize(){
  while read -r line
  do
    gid=$(echo $line | cut -d: -f3)
    if [ $gid -ge 0 -a $gid -le 100 ]; then
      gidsize $gid
    fi
  done < /etc/group
}
