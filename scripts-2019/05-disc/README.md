# @edt ASIX M01_ISO Curs 2018-2019
## Administració de disc


#### Exercicis Funcions size

Fes les següents funcions:


 * (1)**fsize** 
   Donat un login calcular amb du l'ocupació del home de l'usuari. Cal obtenir el home del /etc/passwd.

 * (2)**loginargs**
   Aquesta funció rep logins i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsize.
   Verificar que es rep almenys un argument. Per a cada argument verificar si és un login vàlid, si no
   generra una traça d'error.

 * (3)**loginfile**
   Rep com a argument un nom de fitxer que conté un lògin per línia. Mostrar l'ocupació de disc de cada
   usuari usant fsize. Verificar que es rep un argument i que és un regular file.

 * (4)**loginboth**
   Rep com a argument un file o res (en aquest cas es processa stdin). El fitxer o stdin contenen un lògin per línia.
   Mostrar l'ocupació de disc del home de l'usuari. Verificar els arguments rebuts. verificar per cada login
   rebut que és vàlid.

 * (5)**grepgid**
   Donat un GID com a argument, llistar els logins dels usuaris que petanyen a aquest grup com a grup principal.
   Verificar que es rep un argument i que és un GID vàlid.

 * (6)**gidsize**
   Donat un GID com a argument, mostrar per a cada usuari que pertany a aquest grup l'ocupació de disc del seu home.
   Verificar que es rep un argument i que és un gID vàlid.

 * (7)**allgidsize** 
   Llistar de tots els GID del sistema (en ordre creixent) l'ocupació del home dels usuaris que hi pertanyen.

 * (8)**allgroupsize** 
   Llistar totes les línies de /etc/group i per cada llínia llistar l'ocupació del home dels usuaris que hi pertanyen.
   Ampliar filtrant només els grups del 0 al 100.


#### Exercicis fdisk/blkid/fstab

Fes les següents funcions:

 * (9)**fstype** 
   Donat un fstype llista el device i el mountpoint (per odre de device) 
   de les entrades de fstab d'quest fstype.

 * (10)**allfstype**
   LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
   les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.

 * (11)**allfstypeif**
   LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
   les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.
   Es rep un valor numèric d'argument que indica el numéro mínim d'entrades
   d'aquest fstype que hi ha d'haver per sortir al llistat.


