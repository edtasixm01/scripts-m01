#! /bin/bash
# Roberto Martínez
# 07/04/2020
# del_usuari
# Programa que rep un login i elimina tot el que pertany
# al login de l'usuari rebut. Deixa en un fitxer .tar.gz
# tots els fitxers que li pertanyen i genera traça per 
# stderr de tot allò que va eliminant.
# ----------------------------------------------------------------
OK=0
ERR_NARGS=1
ERR_NOLOGIN=2

# 1) validar args
if [ $# -ne 1 ]; then
  echo "Error: num args incorrecte"
  echo "Usage prog user"
exit $ERR_NARGS
fi

# 2) validar login
login=$1
lineUser=""
lineUser=$(grep "^$login:" /etc/passwd 2> /dev/null)
if [ -z "$lineUser" ]; then
  echo "No existeix usuari $login"
  echo "Usage: prog user"
  exit $ERR_NOLOGIN
fi

# 3) eliminar compte usuari
userdel $login

# 4) backup home usuari i eliminar
homeDir=$(echo $lineUser | cut -d: -f6)
tar cvzf $login-home.tgz $homeDir
rm -rf $homeDir && echo "Homedir usuari $login esborrat" >> /dev/stderr

# 5) esborrar mail
rm -rf /var/spool/mail/$login && echo "Mail usuari $login esborrat" >> /dev/stderr

# 6) llistar, guardar, esborrar fitxers que té usuari fora del homedir
tasquesUser=$(find / -user $login -print 2> /dev/null)
tar cvzf $login-fits.tgz $tasquesUser
find / -user $login -delete 2> /dev/null && echo "Fitxers usuari $login" esborrats >> /dev/stderr

# 7) Matar processos de l'usuari
ps -u $login
pkill -U $(grep "^$login:" /etc/passwd | cut -d: -f3) .* && echo "Processos usuari $login finalitzats" >> /dev/stderr

# 7) esborrar tasques impressió
lpq -U $login
lprm -U $login && echo "Tasques impressió usuari $login esborrades" >> /dev/stderr

# 8) esborrar tasques periòdiques i finalitzar
atUser=$(atq $login | tr -s '[:blank:]' ' ' | cut -d' ' -f1)
for tasca in $atUser
do
  atrm $tasca && echo "Tasca puntual $tasca de l'usuari $login esborrada" >> /dev/stderr
done
crontab -u $login
crontab -u $login -r && echo "Tasques periòdiques usuari $login esborrades" >> /dev/stderr
exit $OK

