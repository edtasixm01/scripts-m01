#! /bin/bash
# Roberto Martinez Conejero
# Març 2020 (confinament)
# M01-ASIX
# Exercicis scripts usuaris
# ---------------------------------------------------

# 9) Llista per ordre de gname tots els grups del sistema. Per a cada grup
# capçalera amb el nom del grup i llistat de tots els usuaris que
# ho tenen com a grup principal, ordenat per login.

function showAllGroupMainMembers(){
  llistaGnames=$(cut -d: -f1 /etc/group | sort)
  for gname in $llistaGnames
  do
    gid=$(grep "^$gname:" /etc/group | cut -d: -f3)
    echo "$gname"
    egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1 | sort | sed -r 's/^([^ ]*)/\t\1/g'
  done
  return 0
}


# 11) Nova versió de showAllGroupMembers afegint capçalera amb nom
# del grup i número d'usuaris que ho tenen com a grup principal. Per 
# a cada usuari mostrem login, uid, home i shell.

function showAllGroupMainMembers2(){
  llistaGnames=$(cut -d: -f1 /etc/group | sort)
  for gname in $llistaGnames
  do
    gid=$(grep "^$gname:" /etc/group | cut -d: -f3)
    numUsers=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | wc -l)
    echo "$gname ($numUsers)"
    egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1 | sort | sed -r 's/^([^ ]*)/\t\1/g'
  done
  return 0
}


# 18) Processa fitxer de text o stdin que conté o espera un gid per línia. Per
# a cada gid mostra el llistat d'usuaris que el tenen com a grup principal
# mostrant login, uid, home.

function showGidMembers(){
  status=0
  ERR_NOGID=1
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then
    fileIn=$1
  fi
  while read -r gid
  do
    line=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group)
    if [ $? -ne 0 ]; then
      echo "GID $gid no existeix al sistema" >> /dev/stderr
      status=$ERR_NOGID
    else
      echo "GID: $gid"
      egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6 2> /dev/null
    fi
  done < $fileIn
  return $status
  }


# 19) Versió de la funció showGidMembers però mostrant només els grups que
# tenen al menys 3 usuaris com a grup principal.

function showGidMembers2(){
  status=0
  ERR_NOGID=1
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then
    fileIn=$1
  fi
  while read -r gid
  do
    line=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group)
    if [ $? -ne 0 ]; then
      echo "GID $gid no existeix al sistema" >> /dev/stderr
      status=$ERR_NOGID
    else
      numUsers=0
      numUsers=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | wc -l)
      if [ $numUsers -ge 3 ]; then
        echo "GID: $gid"
        egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6 2> /dev/null
      fi
    fi
  done < $fileIn
  return $status
}


# 20) Donat un número d'oficina com a argument (validar argument i que oficina
# sigui vàlida, obtenir els codis de rep. ventas que treballen en aquella
# oficina i llistar les comandes de cada venedor.
# Els fitxers estan previament normalitzats amb separador ':'

function showPedidos(){
  OK=0
  ERR_NARGS=1
  ERR_NOOFI=2
  # validem argument
  if [ $# -ne 1 ]; then
    echo "Error: num args incorrecte"
    echo "Usage: function oficina"
    return $ERR_NARGS
  fi
  # validem oficina vàlida
  oficina=$1
  egrep "^$oficina:" oficinas.dat &> /dev/null
  if [ $? -ne 0 ]; then
    echo "Error: oficina $oficina no existeix"
    echo "Usage: function oficina"
    return $ERR_NOOFI
  fi
  # rep que treballen a la oficina
  llistaRep=$(egrep "^[^:]*:[^:]*:[^:]*:$oficina:" repventas.dat | cut -d: -f1 2> /dev/null)
  # Per a cada rep mostrem comandes
  echo "OFICINA $oficina"
  for rep in $llistaRep
  do
    echo "Venedor num: $rep" | sed -r 's/^([^ ]*)/  \1/g'
    egrep "^[^:]*:[^:]*:[^:]*:$rep:" pedidos.dat | cut -d: -f1 | sed -r 's/^([^ ]*)/    \1/g' 2> /dev/null
  done
  return $OK
  }


# 21) Funció que rep un login i mostra per stdout el home de l'usuari.
# Retorna 0 si el troba i sino un valor diferent de zero.

function getHome(){
  login=$1
  egrep "^$login:" /etc/passwd | cut -d: -f6 2> /dev/null
  return $?
  }


# 22) Funció que rep un o més logins i per cadascun mostra el seu
# home utilitzant getHome. Validar arguments.
  
function getHomeList(){
  llistaLogins=$*
  for login in $llistaLogins
  do
    getHome $login 
  done
}


# 23) Donat un homedir com a argument mostra l'ocupació
# en bytes. Cal validar que el homedir existeixi. Retorna 
# 0 si existeix i un altre valor sino existeix.

function getSize(){
  home=$1
  du -sk $home | cut -f1 
  return $?
}


# 24) Idem que l'anterior però processa un a un els logins
# que rep per stdin. Valida que login existeixi sino error.
# Donat el login mostra el size del seu home per stdout.

function getSizeIn(){
  while read -r login
  do
    home=$(getHome $login)
    getSize $home
  done
}

# 25) Processa una a una les línies del fitxer /etc/passwd
# i per a cada usuari mostra el size del seu home.

function getAllUsersSize(){
  while read -r line
  do
    login=$(echo $line | cut -d: -f1)
    home=$(getHome $login)
    getSize $home
  done < /etc/passwd
}
