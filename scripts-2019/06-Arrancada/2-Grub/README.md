# @edt ASIX M01-ISO
## Curs 2019-2020

Podeu trobar els programes al GitLab de [edtasixm01](https://gitlab.com/edtasixm01/scripts-2019.git
)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)

ASIX M06-ASO Escola del treball de barcelona


### Grub


#### Instal·laciuó de Fedora 27 en un HD buit  [part1]

Aquesta part1 descriu el procés d'instal·lació de Fedora 27 en un disc dur *buit*
de tipus qcow2 de 10GB. Primer es genera l'espai buit amb *qemu-img* i s'engega
qemu amb la imatge iso de Fedora Workstation com a cdrom.


Vídeo: [part1](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/2-Grub/Arrancada-Grub-part1-Instal%C2%B7lacio_F27.webm)

Procediment: (7 minuts)
 
 * Assegurar-se de tenir instal·lat @Virtualization.
 * Assegurar-se de tenir engegat el servei libvirtd.

 * Descarregar la imatge de instal·lació de Fedora 27 Workstation Netinst, que descarrega els paquets de xarxa.
 * Generar amb *qemu-img* un disc dur de 10 GB on farem la instal·lació: hisx1-grub.qcow2.
 * Engegar *qemu-kvm* indicant com a hda el disc creat i com a cdrom la iso d'instal·lació.
 * Seguir el procediment usual d'instal·lació. 
 * Generar: sda1 de 3GB on instal·lar F27. Cap altra partició.


#### Menús i submenus de Grub  [part2]

Aquesta part2 descriu un mecanisme bàsic *i amb trampa* de generar menús i submenús de grub.
Genera un fitxer 'pelat' amb les instruccions elementals d'una entrada de grub i replica
aquest menú amb opcions diferents. Finalment les agrupa en un submenú

Vídeo: [part2](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/2-Grub/Arrancada-Grub-part2-menus_grub.webm)

Continuació: (17 minuts, però es poden fer molts salts!)

 * Generar una copia de backup de grub.cfg.
 * Generar un fitxer *pelat* amb una sola entrada amb els ítems imprescindibles.
 * Generar múltiples entrades amb opcions diferents.
 * Generar un submenú.


#### Mode comanda per a recuperació d'errors: falta fitxer grub.cfg  [part3]

Aquesta part3 es presenten les diverses opcions que permet el mode comanda per engegar el sistema, en cas
de que no es pugui engegar correctament, per exemple perquè falta el fitxer grub.cfg.

Vídeo: [part3](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/2-Grub/Arrancada-Grub-part3-menus-comanda-rescat.webm)

Continuació: (14 minuts)

 * Mode comanda del grub.
 * Llistar particions, directoris i fitxers des del mode comanda.
 * esborar el fitxer grub.cfg, l'arrancada queda clavada!.
 * Recuperació: escriure línia a línia les instruccions com si d'una entrada de menú es tractés. 
   Usar *linux16* i *initrd16*.
 * Recuperació: usar *configfile* per seleccionar del disc dur un dels fitxers de grub vàlids.
 * Recuperació: usar *normal* (no va?).
 * Entendre el significat de *set root* i de *set prefix*.
 * Directori de mòduls: */boot/grub2/i386-pc*.
 * Fitxer de kernel i fitxer de ram disk.
 * Generar un menú automàticament amb *grub2-mkconfig*


#### Instal·lar Fedora 30 en un snapshot  [part4]

Aquesta part4  genera un *snapshot* de la imatge de les pràctiques anteriors (amb un F27) i genera a la
VM una nova partició sda2 on hi instal·la un Fedora 30. D'aquesta manera la imatge base té només
la instal·lació del F27 i la imatge derivada (snapshot) té els dos sistemes.

Vídeo: [part4](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/2-Grub/Arrancada-Grub-part4-instal%C2%B7lar_F30_snapshot.webm)

Continuació: (11 minuts)

 * Generar un snapshor de la imatge base *hisx1-grub.qcow2* anomenat *hisx1-dual.qcow2*.
 * En aquesta nova imatge *hisx1-dual.qcow2* instal·lar-hi el Fedora Workstation 30 Netinst.
 * Seguir el procediment d'instal·lació usual per a una instal·lació *minimal*.
 * Generar una partició sda2 de 2GB on s'hi instal3la el Fedora Workstation 30 Minimal.


#### Arrancar manualment altres particions / Determinar qui mana   [part5]

Aquesta part5 es troba amb algunes dificultats perquè el comportament del Grub del F30 és
diferent de l'esperat. Això permet practicar com arrencar una partició que no està llistada 
als menús (amb linux16/inird16, amb configfile o amb normal).

També es practica com estblir quina és la partició que mana, amb l'ordre *grub2-install*. 
Podem establir que maní la partició activa actual o una altra partició (que cal carregar prèviament
amb mount).

Vídeo: [part5](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/2-Grub/Arrancada-Grub-part5-determinar_qui_mana.webm)

Continuació: (15 minuts)

 * Observar qu eel F30 no ha generat un menú del grub complert i correcte ja que no mostra les entrades del F27.
 * Des d'un grub que no mostra entrades per arrencar F27 arrencar-lo igualment:
 * Recuperació: manualment indicant el kernel i el initramfs amb les ordres *linux16* i *initrd16*.
 * Recuperació: manualment amb *configfile*.
 * Recuperació: manualment amb *normal*.
 * Determinar qui mana, és a dir, a on apunta la part d'arrancada del grub del MBR.
 * MBR || sda1 f27 || sda2 f30
 * MBR --------------->
 * Modificar amb l'ordre *grub2-install* el MBR reescrivint el grub fent que apunti a la partició actual.
 * MBR || sda1 f27 || sda2 f30
 * MBR ---->
 * Generar menús automatitzats amb *grub2-mkconfig*.


#### Establir que mani el Grub d'una altra partició   [part6]

En aquesta part6 es fan els exemples per mostrar com modificar quina partició del grub mana (on apunta MBR) 
indicant que mana una partició diferent de la partició activa. Per exemple des del F30 establir que mani la
partició del F27. S'utilitza l'opció *--root-directory* de l'ordre *grub2-install*. Prèviament cal muntar,
per exemple a mnt, la partició que ha de manar.


Vídeo: [part6](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/2-Grub/Arrancada-Grub-part6-mana_altra_partici%C3%B3.webm)

Continuació: (8 minuts)

 * Des de F30 establir que mani F27: primer es munta sda1 a /mnt i després es fa l'ordre *grub-install*.
 * Des de F27 establir que mani F30 (no funciona bé!).
 * Fet manualment pas a pas, reboot per engegar F30 i establir que mani la partició activa.


#### Moure una partició: F30 de sda2 a sda3   [part7]

En aquesta part7 es fa un snapshot de la imatge amb la que es treballava fins ara, preservant la imatge
*hisx1-dual.qcow* amb l'arrancada dual i generant una nova imatge on es moura el F30 de sda2 a sda3. 
El procediment am fa amb  dd i de fet podeu obviar el vídeo a partir del minut 5!.

Vídeo: [part7](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/2-Grub/Arrancada-Grub-part7-snapshot_moure_particio.webm)

Continuació: (5 minuts, **ignoreu tot el que va després del minut 5**)

 * MBR || sda1 f27 || sda2 F30
 * MBR ----------------->
 * Volem moure F30 de sda2 a sda3. En fer-ho l'arrencada quedarà corrupta 8i no és política!)
 * MBR || sda1 f27 || sda2 //lliure// || sda3 F30
 * MBR ----------------->X
 * El prodeciment consistirà en crear la partició sda3, moure amb *dd* sda2 a sda3 i planxar zeros a sda2.


#### Recuperar el grub quan s'ha mogut la partició on apuntava   [part8]

En aquesta part8 s'acaba de moure f30 de sda2 a sda3 i en fer reboot s'observa que el boot està trencat,
el MBR apunta a una partició (sda2) que no conté res. No estem en mode comanda usual sinó en un 
*mode comanda rescue* molt més limitat d'opcions.

Amb els *trucs* usuals engegar un dels dos sistemes i reescriure el grub al MBR. Podem fer-ho usant
les ordres *linux16*+*intrd16*, l'opció de *configfile* o l'opció *normal*.

Vídeo: [part8](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/2-Grub/Arrancada-Grub-part8-recuperaci%C3%B3_partici%C3%B3_moguda.webm)

Continuació: (14 minuts, **ignoreu  els primers 5:48 minuts primers!**)

 * MBR || sda1 f27 || sda2 F30
 * MBR ----------------->
 * Volem moure F30 de sda2 a sda3. En fer-ho l'arrencada quedarà corrupta (i no és política!)
 * MBR || sda1 f27 || sda2 //lliure// || sda3 F30
 * MBR ----------------->X
 * Un cop fet reboot el grub està trencat i genera un mode comanda de *rescue* molt bàsic.
 * Engegar algun dels dos sistemes: F27 o F30 i reescriure el grub al MBR.


#### Recuperar-se d'un MBR esborrat   [part9]

En aquesta part9 es veuran opcions per recuperar-se d'un MBR esborrat. S'ha esborrat
accidentalment o hem tingut la trista idea d'instal·lar un windows que ha eliminat el Grub per 
posar-hi el seu  carregador d'arrencada i ara no podem engegar els Linux.

Aquí esborrarem expressament el MBR xafant amb zeros els 512 primers bytes del disc dur. Això no només
elimina l'arrancada del Grub sinó també la Taula de particions. Es crearà de nou la taula de 
particions i si els sectors d'inici de cada partició coincideixen amb els antics per art de màgia 
es recuperaran les dades. Per crear de nous les particions s'engegarà amb alguna utilitat Live,
per exemple la pròpia ISO d'instal·lació de Fedora.

El disc, però, encara no podrà engegar perquè no hi ha arrencada. Caldrà usar una Live que permeti 
resscriure el grub al MBR. Aquó enegarem el *super-grub2-disk* que detecta els grubs que hi ha al disc
dur i permet engegar-los. Un cop iniciat un dels dos sistemes ja es pot regenerar el grub al MBR.

Vídeo: [part9](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/2-Grub/Arrancada-Grub-part9-recuperaci%C3%B3_MBR_esborrat.webm)

Continuació: (11 minuts)

 * Esborrar el MBR escrivint zeros als 512 primer bytes del HD.
 * Arrencar amb un sistema Live per refer les particions. Usem Fedora Workstation Netinst com si 
   anèssim a fer una instal·lació i des de la consola F2 (sendkey ctrlált-f2) creeem de nou les
    particions, respectant els valors inicials.
 * Un cop les particions són vàlides arrenquem de nou ara amb el *super-grub2-disc* Live perquè
   detecti les configuracions de grub i els linux que hi ha al HD. Podem escollir si engegar F27
   o F30.
 * Un cop s'ha iniciat correctament un sistema reescriure el grub al MBR amb l'ordre *grub2-install*





















