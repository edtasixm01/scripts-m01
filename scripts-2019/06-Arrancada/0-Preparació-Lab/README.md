# @edt ASIX M01-ISO
## Curs 2019-2020

Podeu trobar els programes al GitLab de [edtasixm01](https://gitlab.com/edtasixm01/scripts-2019.git
)

Podeu trobar la documentació del mòdul a [ASIX-M01](https://sites.google.com/site/asixm01edt/)

ASIX M06-ASO Escola del treball de barcelona


### Preparació de labotatori de treball / Màquines Virtuals


#### Arrancada d'una imatge Fedora Cloud  [part1]

Aquesta part1 descriu com obtenir les imatges de Fedora Cloud del repositori
archives de fedora per engegar-les amb qemu-kvm. Abans, però cal fer una petita
'preparació' amb *virt-sysprep* per assignar-li password a root.

Vídeo: [part1](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/0-Preparaci%C3%B3-Lab/Arrancada-Lab-part1-Fedora_Cloud.webm)

Procediment: (4 minuts)
 
 * Assegurar-se de tenir instal·lat @Virtualization.
 * Assegurar-se de tenir engegat el servei libvirtd.

 * Descarregar la imatge FedoraCloud desitjada (preferentment qcow2).
 * Amb *virt-sysprep* assignar-li un password.
   [ a partir del minut 4 es pot ignorar: és un fracàs!]


#### Arrancada d'una imatge Fedora Cloud  [part2]

Vídeo: [part2](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/0-Preparaci%C3%B3-Lab/Arrancada-Lab-part2-Fedora_Cloud.webm)

Continuació:  (9 minuts)

 * Amb *virt-sysprep* assignar-li un password.
 * Generar una imatge de disc hisx1-treball.qcow2  de 10GB buida.
 * Amb *virt-resize* planxar la imatge Fedora Cloud a la imatge buida anterior.
   Ara ja tenim un VM amb dues particions, sda1 (4GB) amb FedoraClud i sda2 (6GB) buida.
 * Engegar la VM amb *qemu-kvm*.
   [ a partir del minut 9 es pot ignorar ]


#### Arrancada de una VM  amb imatges de ISO LIVE cdrom [part3]

Com engegar imatges ISO Live conjuntament amb la VM creada en els apartats
anteriors. Imitant el procediment d'arrancada amb CDROM o USB per a discs de
utilitats com Gparted o Super-Grub2, o ISOs de instal·lacions.

Vídeo: [part3](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/0-Preparaci%C3%B3-Lab/Arrancada-Lab-part3-Imatges_Live.webm)

Continuació:  (11 minuts)

 * Verificar que engega la VM realitzada en la part2.
 * Engegar la VM arrancant des de una ISO LIVE de  Gparted.
 * Engegar la VM arrancant des de una ISO LIVE de Super Gurb2 Disk.
 * Identificar les webs de descàrrega de gparted o super-grub.
 * Engegar la VM arrencant una ISO Live de instal·lació de Fedora 30 Netinst (via xarxa).


#### Instal·lar un segon sistema, un Fedora 30 [part4]

Tenim la VM que s'ha generat en la part1 i ara s'instal·larà a l'espai disponible
(esborrant sda2) un segon sistema operatiu un Fedora 30. S'utilitza la ISO
de Netinstall que permet la instal·lació obtenint els paquets de xarxa. Es
realitza una instal3lació mínima (1.1GB).

Vídeo: [part4](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/0-Preparaci%C3%B3-Lab/Arrancada-Lab-part4-Instal%C2%B7lar_F30_dual.webm)

Continuació:  (8 minuts)

 * Descarregar la iso Fedora Workstation 30 Netinstall.
 * Iniciar la VM indicant que el primer cop engegi de cdrom.
 * Realitzar el procés habitual d'instal·lació.
 * Seleccionar idioma, teclat, installation source...
 * Assegurar-se de selecionar instal·lació Minimal.
 * Com accedir a la consola i canviar de consola amb Qemu: 
   s'utilitza l'ordre sendkey ctrl+alt-f2 (des de la consola qemu).
 * Crear les particions: sda2 1GB lliure, sda3 3GB pel F30.
 * Seleccionar el destí de la instal·lació (sda3 ext4 / F30)
 * Posar password de root i compte guest.


#### Utilitzar Snapshots [part5]

Ara tenim una VM amb dos sistemes operatius F27 (sda1 4GB) i F30 (sda3 3GB),
podem generar un snapshot per mantenir la imatge base inalterada i fer totes les
prtiques perilloses en el snapshot. Si 'casca' sempre podem tornar a la imatge base.

Vídeo: [part5](https://gitlab.com/edtasixm01/scripts-2019/-/blob/master/06-Arrancada/0-Preparaci%C3%B3-Lab/Arrancada-Lab-part5-Snapshot.webm)

Continuació:  (13 minuts)

 * Verificar l'arrancada de F30.
 * Mofidicar el menú de grub per regenerar-lo mostrant les entrades duals.
 * Generació d'un snapshot de la VM (usant de base hisx1-treball.qcow2) generant
   hisx1-snapshot.qcow2.
 * Utilització de snapshots per fer r transformacions que podem descartar i tornar a
   la imatge base.
 * Demostració de modificar el grub del snapshot i observar que la base no s'ha modificat.














