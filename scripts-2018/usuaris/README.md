# @edt ASIX M01-ASO Curs 2018-2019
## Funcions d'usuari

Funcions Generals:

 * hola
 * suma

Funcions bàsiques usuaris:

 * showUser
 * showUserGecos
 * showGroup

Funcions ampliades:

 * showUser
 * showUserList
 * showuserIn
 * showGroupMainMembers

Funcions de llistat/informes:

 * showAllShells
 * showAlGroups


Funcions de shadow / crear / esborrar:

  * showShadow

## Creació /eliminació automatitzada d'usuaris

  * **killuser.sh**

  Programa que rep un login i 'elimina' l'usuari  segons el criteri següent:

    * si no hi ha argument error.

    * si no existeix l'usuari error.

    * eliminar el compte d'usuari.

    * fer un tar/gz del home de l'usuari i eliminar el home.

    * incloure al tar/gz el mbox de l'usuari.

    * eliminar (llistar)  tots els fitxers de l'usuari en el sistema (find).

    * eliminar (llistar) tots els processos de l'usuari en el sistema (ps/kill).

    * eliminar (llistar) tots els treballs d'impressió de l'usuari (lp/lprm).

    * eliminar totes les tasques periòdiques/puntuals (at/cron).

  * **CrearClasse.sh**

  Programa que rep com a argument el nom de un curs/classe, per exemple 
  hisx1 o carnicers3. Ha de crear el grup (no ha d'existir), el seu home 
  base (/home/nom-del-curs) i 30 usuaris amb noms tipus 
  hisx1-01..hisx1-30.


