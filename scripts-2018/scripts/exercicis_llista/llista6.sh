#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# Adrián Narváez isx39448945
# Descripcio: Processar per stdin linies d̉entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.
# Synopsis: prog.sh
OK=0
while read -r line
do
  nom=$(echo $line | cut -c1)
  cognom=$(echo $line | cut -d' ' -f2)
  echo "$nom. $cognom"
done
exit $OK

