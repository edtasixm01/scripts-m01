#! /bin/bash
# @edt ASIX M01 CURS 2018-2019
# exercici 7
# 25/02/2019
# ------------------------------------------------------------------------------
#Validar args
status=0
if [ "$#" -ne 5 ]
then
  echo "ERROR numero args"
  echo "usage: 07_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARG
fi
if [ "$1" != "-f" -a "$1" != "-d"  ]
then
  echo "ERROR format arguments"
  echo "usage: 07_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARG
fi
tipus=$1
shift
for arg in $*
do
  if ! [ $tipus "$arg" ]; then
    status=2
  fi
done
exit $status


