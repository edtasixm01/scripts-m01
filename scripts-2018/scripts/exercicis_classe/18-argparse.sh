#! /bin/bash
# @edt ASIX M01-ISO Curs 2018-2019
# 
# prog -a file -b -d -d num -e arg[...]
# -------------------------------------
opcions=""
arguments=""
fitxer=""
num=""
while [ -n "$1" ]
do
  case $1 in 
  -a)
    fitxer=$2
    shift;;
  -d)
    num=$2
    shift;;    
  -b|-c|-e)
    opcions="$opcions $1";;
  *)
    arguments="$arguments $1";;
  esac
  shift
done
echo "opc: $opcions"
echo "args: $arguments"
echo "file: $fitxer"
echo "num: $num"
