#! /bin/bash
# @edt ASIX M01-ISO Curs 2018-2019
# prog [ -a -b- -c- -d- -e] arg[...]
# separar en dues llistes args i opcions
# ------------------------------------------
opcions=""
arguments=""
for arg in $*
do
  case $arg in
  -a|-b|-c|-d|-e)
    opcions="$opcions $arg";;
  *)
    arguments="$arguments $arg";;
  esac
done
echo "opcions: $opcions"
echo "arguments: $arguments"
