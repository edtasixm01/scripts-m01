#! /bin/bash
# @ edt ASIX-M01 Curs 2022-2023
# Febrer 2022
# Validar nota: suspès, aprovat
# -------------------------------
ERR_NARGS=1
ERR_NOTA=2
# 1) si num args no es correcte plegar
if [ $# -ne 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi	
# 2) Validar rang nota
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
  echo "Error nota $1 no valida"
  echo "nota pren valors de 0 a 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi	
# Xixa
nota=$1
if [ $nota -lt 5 ]; then
  echo "nota $nota: suspès"
else
  echo "nota $nota: aprovat"
fi
exit 0
