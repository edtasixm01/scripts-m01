#! /bin/bash
# @edt ASIX-M01 Curs 2022-2023
# Febrer 2023
# Descripcio: exemples bucle for
# ---------------------------------

# 8) Llistar tots els logins numerats
llista_logins=$(cut -d: -f1 /etc/passwd | sort)
num=1
for login in $llista_logins
do
  echo "$num: $login"
  ((num++))
done	
exit 0 

# 7) llistar numerats els noms dels files
#    del directori actiu
llista_noms=$(ls)
num=1
for nom in $llista_noms
do
  echo "$num: $nom" 
  ((num++))
done
exit 0


# 6) numerar els arguments
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))
done
exit 0


# 5) Iterar i mostrar la llista d'arguments
for arg in "$@"
do
  echo $arg
done
exit 0


# 4) Iterar i mostrar la llista d'arguments
for arg in $*
do
  echo $arg	
done
exit 0

# 3) iterar pel valor d'una variable
llistat=$(ls)
for nom in $llistat
do
  echo "$nom"	
done	
exit 0




# 2) iterar per un conjunt d'elements
for nom in "pere marta pau anna"
do
  echo "$nom"   
done    
exit 0


# 1) iterar per un conjunt d'elements
for nom in "pere" "marta" "pau" "anna"
do
  echo "$nom"  	
done	
exit 0

