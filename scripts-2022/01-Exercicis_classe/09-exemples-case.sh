#! /bin/bash
# @edt ASIX M01-ISO
# Curs 2023-2023
# Exemples case
# -------------------

# dl dt dc dj dv ---> laborable
# ds dm --> festiu
# altres
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
    # d[ltcjv]	  
    echo "$1 és un dia laborable";;
  "ds"|"dm")
    # d[sm]	  
    echo "$1 és festiu";;
  *)
    echo "això ($1) no és un dia";;
esac
exit 0

case $1 in
  [aeiou]) 
    echo "és una vocal"
    ;;
  [bcdfghjklmnpqrstvwxyz])
    echo "és una consonant"
    ;;
  *)    
    echo "és una altra cosa"
    ;;
esac	
exit 0

case $1 in
  "pere"|"pau"|"joan")
    echo "és un nen"
    ;;
  "marta"|"anna"|"julia")
    echo "és una nena"
    ;;
  *)
    echo "no binari"
    ;;    
esac
exit 0

