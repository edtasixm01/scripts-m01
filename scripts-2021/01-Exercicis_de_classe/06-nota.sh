#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# Validar nota: suspes, aprovat, notable, excel·lent
# -------------------------------
# si num args no es correcte plegar
ERR_ARGS=1
ERR_NOTA=2
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 nota"
  exit $ERR_ARGS
fi
# si nota no és [0-10] plegar
if ! [ $1 -ge 0 -a $1 -le 10 ]; then
  echo "ERROR: nota $1 novalida [0-10]"
  echo "usage: $0 nota"
  exit $ERR_NOTA
fi
# xixa
nota=$1
if [ $nota -lt 5 ]; then
  echo "La nota $nota es un Suspes"
elif [ $nota -lt 7 ]; then
  echo "la nota $nota es un Aprovat"
elif [ $nota -lt 9 ]; then
  echo "La nota $nota es un Notable"
else
  echo "La nota $nota es un Excellent"
fi
exit 0
