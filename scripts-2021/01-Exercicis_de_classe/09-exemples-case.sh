#! /bin/bash
# @edt ASIX M01-ISO
# Curs 2021-2022
# Exemples case
# -------------------

# dl dt dc dj dv laborable
# ds dm festiu
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
    echo "$1 és un dia laborable";;
  "ds"|"dm")
    echo "$1 és festiu";;
  *)
    echo "això ($1) no és un dia";;
esac
exit 0

case $1 in
  "d[lmcjv]")
    echo "$1 és un dia laborable";;
  "d[sm]")
    echo "$1 és festiu";;
  *)
    echo "això ($1) no és un dia";;
esac
exit 0

case $1 in
  [aeiou])
     echo "$1 és una vocal";;
  [bcdfghjklmnpqrstvwxyz])
     echo "$1 és una consonant";;
  *)
     echo "$1 és una altra cosa"
esac
exit 0

case $1 in
  "pere"|"pau"|"joan")
     echo "és un nen"
     ;;	  
  "marta"|"anna"|"julia")
     echo "és una nena"
     ;;
  *)
     echo "és indefinit"
     ;;
esac
exit 0


