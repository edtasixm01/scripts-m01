#! /bin/bash
# @edt Curs 2021-2022
# copy.sh file dir-destí
# validar dir-destí és dir i existeix
# validar file regular file
# ----------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOREGULARFILE=3
OK=0
#1) Validar nargs
if [ $# -ne 2 ]; then
  echo "Error: num args incorrecte"
  echo "usage $0 file[...] dir-destí"
  exit $ERR_NARGS
fi
file=$1
desti=$2
# 2) dir-desti existeix
if [ ! -d $desti ]; then
  echo "Error: $desti no és un directori"
  echo "usage $0 file dir-destí"
  exit $ERR_NODIR
fi
# 3) file és un regular file existent
if [ ! -f $file ]; then
  echo "Error: $file no és un regular file"
  echo "usage $0 file[...] dir-destí"
  exit $ERR_NOREGULARFILE
fi
cp $file $desti
exit 0

