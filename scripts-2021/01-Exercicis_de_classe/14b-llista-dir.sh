#! /bin/bash
# @edt ASIX M01-ASO Curs 2021-2022
# Febrer 2022
# $ llistar-dir.sh dir
#  ampliat llistant i numerant elements
# -------------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) validar arguments
if [ $# -ne 1 ]
then
  echo "Error: número args no vàlid"
  echo "usage: $0 dir"
  exit $ERR_NARGS
fi
dir=$1

# 2) validar arg és un dir
if ! [ -d $dir ]
then
  echo "Error: $dir no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi
# 3) xixa
llista_dir=$(ls $dir)
num=1
for nom in $llista_dir
do
  echo "$num: $nom"
  ((num++))
done	
exit 0


