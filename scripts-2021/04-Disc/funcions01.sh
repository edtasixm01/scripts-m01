#! /bin/bash
# @edt Curs 2021-2022
# ASIX M01-ISO
# Exemples de funcions


function hola(){
   echo "hola"
   return 0
}

function dia(){
   date
   return 0   
}

function suma(){
  echo $(($1+$2))
  return 0
}

function all(){
  hola
  echo "avui som: $(dia)"
  suma 6 9
}  

# (1)fsize
# Donat un login calcular amb du l'ocupació 
# del home de l'usuari. Cal obtenir el home
# del /etc/passwd.
function fsize(){
    #1) validar existeix login
    login=$1
    linia=$(grep "^$login:" /etc/passwd)
    if [ -z "$linia" ]; then
       echo "Error: user $login inexistent"
       return 1       
    fi	    
    #2) Obtenir el home
    dirHome=$(echo $linia | cut -d: -f6)
    #3) calcular du -sh
    du -sh $dirHome
    return 0
}

# (2) loginargs
# rep logins i per a cada login es mostra 
# l'ocupació de disc del home de l'usuari
# usant fsize.
function loginargs(){
    #1) validar almenys es rep 1 login
    if [ $# -eq 0 ]; then
       echo "error..."
       return 1       
    fi	    
    #2) iterar per cada login
    for login in $*
    do
        fsize $login
    done	
}	

# 3) loginfile
# Rep com a argument un nom de fitxer que conté 
# un lògin per línia. Mostrar l'ocupació de disc
# de cada usuari usant fsize. Verificar que es 
# rep un argument i que és un regular file.
function loginfile(){
    # Validar existeix fitxer
    if [ ! -f "$1" ]; then
         echo "fitxer inexistent"
         return 1	 
    fi	 
    fileIn=$1
    while read -r login
    do
       fsize $login
    done < $fileIn
}

# (4) loginboth
#     loginboth [file]
# processa file o stdin, mostra fsize 
# dels users
function loginboth(){
    fileIn=/dev/stdin
    if [ $# -eq 1 ]; then
        fileIn=$1	    
    fi	    
    while read -r login
    do
        fsize $login	    
    done < $fileIn
}

# (5) grepgid gid
# validar rep un arg
# validar que és un gid vàlid
# retorna la llista de logins que tenen 
# aquest grup com a grup principal
function grepgid(){
    if [ $# -ne 1 ]; then
        echo "error args..."
        return 1	
    fi	    
    gid=$1
    grep -q "^[^:]*:[^:]*:$gid:" /etc/group
    if [ $? -ne 0 ]; then
        echo "error gid $gid inexistent"
	return 2
    fi	    
    cut -d: -f1,4 /etc/passwd | grep ":$gid$"         | cut -d: -f1
}


# #(6) gidsize gid
# Donat un GID com a argument, mostrar
# per a cada usuari que pertany a aquest
# grup l'ocupació de disc del seu home.
function gidsize(){
    gid=$1
    llista_logins=$(grepgid $gid)
    for login in  $llista_logins
    do
        fsize $login
    done	    
}  


# (7) allgidsize
# Infome de tots els gids llistant
# l'ocupació dels usuaris que hi pertanyen
function allgidsize(){
    llista_gids=$(cut -d: -f4 /etc/passwd | sort -gu)
    for gid in $llista_gids
    do	
	echo "GID: $gid --------------"
	gidsize $gid
    done	
}


#(8)  filterGroup 
# llistar linies del /etc/passwd 
# dels usuaris dels grups [0-100]
function filterGroup(){
    file=passwd.txt	
    while read -r line
    do
        gid=$(echo $line | cut -d: -f4)
	if [ $gid -ge 0 -a $gid -le 100 ]; then
            echo $line		
        fi		
    done < $file
}





