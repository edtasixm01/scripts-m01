#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# exercici 10
###################################################################
status=0
while read -r gid
do 
  groupLine=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
  if [ $? -eq 0 ]; then
    gname=$(echo $groupLine | cut -d: -f1 | tr '[a-z]' '[A-Z]')
    llistaUsers=$(echo $groupLine | cut -d: -f4 | tr '[a-z]' '[A-Z]')
    echo "gname: $gname, gid: $gid, users: $llistaUsers"
  else
    echo "Error $gid inexistent!" >> /dev/stderr    
    status=1
  fi    
done
exit $status
